<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_flights', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('booking_id');
            $table->json('booking_data');
            $table->json('passenger_data');
            $table->json('review_data');
            $table->json('retreive_data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_flights');
    }
}
