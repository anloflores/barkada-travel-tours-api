<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        

        Passport::tokensCan([
            'can-book-flights' => 'Can Book Flights',
            'can-book-hotels' => 'Can Book Hotels',
            'can-search-flights' => 'Can Search Flights',
            'can-search-hotels' => 'Can Search Hotels',
            'can-transact' => 'Can Deduct to Wallet',
        ]);

        Passport::setDefaultScope([
            'can-book-flight',
            'can-book-hotel',
            'can-search-flight',
            'can-search-hotels',
            'can-transact'
        ]);
    }
}
