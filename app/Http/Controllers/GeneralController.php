<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function countries() {
        $data      = json_decode(file_get_contents(public_path('countries.json')));

        return response()->json(collect($data)->sortBy('name'), 200);
    }
}
