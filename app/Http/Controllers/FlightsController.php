<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

class FlightsController extends Controller
{

    public function autofill(Request $request) {
        $validator = Validator::make($request->all(), [
            'term' => 'required'
        ]);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://ph.via.com/apiv2/flight/airports-auto/'. $request->term .'?&flowType=NODE&ajax=true&jsonData=true');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Via-Access-Token:c51b5435-cec6-4de2-b133-419139122a02',
            'Content-Type:application/json',
            'Referrer:https://ph.via.com/'
        ));
        $content = curl_exec($ch);
        curl_close($ch);

        return response()->json(json_decode($content));
    }

    public function search(Request $request) {
        $validator = Validator::make($request->all(), [
            'from' => 'required',
            'to'   => 'required',
            'adt' => 'required',
            'chd' => 'required',
            'inf' => 'required',
            'dd' => 'required',
        ]);
        
        if($validator->fails()) return $validator->errors();

        $formatDate = function($date) {
            $arrDate = explode('-', $date);
            return $arrDate[2] . '-' . $arrDate[0] . '-' . $arrDate[1];
        };

        $sectors = [];
        $sectors[] = [
                "src" => [
                    "code" => $request->input('from')
                ],
                "dest" => [
                    "code" => $request->input('to')
                ],
                "date" => date('Y-m-d', strtotime($formatDate($request->input('dd'))))
            ];

        if($request->has('rt') && $request->rt) {
            $sectors[] = [
                    "src" => [
                        "code" => $request->input('to')
                    ],
                    "dest" => [
                        "code" => $request->input('from')
                    ],
                    "date" => date('Y-m-d', strtotime($formatDate($request->input('rd'))))
                ];
        }

        $data = [
            "sectorInfos" => $sectors,
            "class" => $request->st,
            "paxCount" => [
                "adt" => $request->input('adt'),
                "chd" => $request->input('chd'),
                "inf" => $request->input('inf')
            ],
            "route" => "ALL"
        ];


        // return $data;

        // $request->session()->put('search_form_data', $data);
        $results = $this->_requestToVia('flight/search', $data);
        return response()->json($results, 200);
    }

    public function review(Request $request) {
        $validator = Validator::make($request->all(), [
            'keys' => 'required',
        ]);
        
        if($validator->fails()) return $validator->errors();
        
        $data = [
            "keys" => $request->keys,
            "isSSRReq" => true
        ];
        
        $review = $this->_requestToVia('flight/review', $data);
        return response()->json($review, 200);
    }

    public function book(Request $request) {

        $validator = Validator::make($request->all(), [
            'ik' => 'required',
            'pd' => 'required',
            'tl' => 'required',
            'keys' => 'required',
            'dd' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        // Re-review the keys for repricing
        // Notify frontend when there's a price changes
        $data = [
            "keys" => $request->keys,
            "isSSRReq" => true
        ];
        
        $review = $this->_requestToVia('flight/review', $data);
        
        if(!isset($review->itinKey))
            return response()->json([
                'msg' => 'Search Expired',
                'code' => 422
            ], 422);

        $noAdt = $review->searchQuery->paxCount->adt;
        $noChd = $review->searchQuery->paxCount->chd;
        $noInf = $review->searchQuery->paxCount->inf;

        $availSSRType = [];
        $availSSRKeys = [];
        $availSSRCode = [];

        $availTitle = $review->titles;
        // return response()->json($review->ssr->ssrHeap, 200);
        
        foreach($review->ssr->ssrHeap as $type => $item) {
            // Put Available SSR Types
            if(isset($item) && count($item) >= 1) {
                if(!in_array($type, $availSSRType)) $availSSRType[] = $type;
                foreach($item as $d) {
                    if(!in_array($d->key, $availSSRKeys)) $availSSRKeys[] = $d->key;

                    if(isset($d->data)) {
                        foreach($d->data as $data) {
                            if(!in_array($data->code, $availSSRCode)) $availSSRCode[] = $data->code;
                        }
                    }
                }
            }
        }

        // Check number of passengers vs inputted per pType
        $inAdt = 0; $inChd = 0; $inInf = 0;
        foreach($request->pd as $pd) {

            // check necessary data
            if(!array_key_exists('title', $pd) || $pd['title'] == '')
                return response()->json([
                    'msg' => 'title is missing or empty',
                    'code' => 422
                ], 422);
            
            $pt = $pd['pType'];
            // Check if title is correct
            if(!in_array($pd['title'], $availTitle->$pt))
                return response()->json([
                    'msg' => 'incorrect title',
                    'code' => 422
                ], 422);

            if(!array_key_exists('firstName', $pd) || $pd['firstName'] == '')
                return response()->json([
                    'msg' => 'firstName is missing or empty',
                    'code' => 422
                ], 422);

            if(!array_key_exists('lastName', $pd) || $pd['firstName'] == '')
                return response()->json([
                    'msg' => 'lastName is missing or empty',
                    'code' => 422
                ], 422);

            if(!array_key_exists('dob', $pd) || $pd['firstName'] == '')
                return response()->json([
                    'msg' => 'dob is missing or empty',
                    'code' => 422
                ], 422);

            // Format date to expected format
            $pd['dob'] = date('Y-m-d', strtotime($pd['dob']));

            switch($pd['pType']) {
                case 'adt': $inAdt++; break;
                case 'chd': $inChd++; break;
                case 'inf': $inInf++; break;
                default:
                    return response()->json([
                        'msg' => 'invalid pType of ' . $pd['pType'],
                        'code' => 422
                    ], 422);
            }

            

            if(isset($pd['ssr'])) {
                foreach($pd['ssr'] as $ssr) {
                    if(!in_array($ssr['ssrType'], $availSSRType)) 
                        return response()->json([
                            'msg' => 'invalid ssr type of ' . $ssr['ssrType'],
                            'code' => 422
                        ], 422);

                    if(!in_array($ssr['key'], $availSSRKeys)) 
                        return response()->json([
                            'msg' => 'invalid ssr key of ' . $ssr['key'],
                            'code' => 422
                        ], 422);

                    if(!in_array($ssr['code'], $availSSRCode)) 
                        return response()->json([
                            'msg' => 'invalid ssr code of ' . $ssr['code'],
                            'code' => 422
                        ], 422);
                }
            }


        }

        // Checks the consistency of search and input
        if($noAdt != $inAdt || $noChd != $inChd || $inInf != $noInf) {
            return response()->json([
                        'msg' => 'inconsistent number of passengers',
                        'code' => 422
                    ], 422);
        }


        // Check the ssr if exists.


        // Book Flight
        $booking_data = [
            "deliveryData" => (array) $request->dd,
            "travellersData" => $request->pd,
            "itinKey" => $request->ik,
            "block" => "false",
            "productType" => "FLIGHT",
            "payment" => [
                "amountToCharge" => $request->tl,
                "itinKey" => $request->ik,
                "productType" => "FLIGHT",
                "paymentSubType" => "3",
                "paymentMode" => "DEPOSIT",
                "paymentDetail" => (object) [],
                "voucher" => (object) [],
                "depositAmount" => 0,
                "voucherAmt" => 0,
                "totalMarkup" => 0
            ]
        ];

        $book = $this->_requestToVia('flight/book', $booking_data);
        if(isset($book->msg)) return response()->json([
            'msg' => $book->msg,
            'reference' => $book->reference
        ], 200); 
        
        if(isset($book->err)) return response()->json($book->err, 200); 

        $reference = $book->reference;
        $retreive = $this->_requestToVia('booking/retrieve/' . $reference, null, 'GET');
        $retreive->bookingUser = [
            "dev" => "carlo.flores@complexus.net"
        ];

        $booking_id = DB::table('bookings')
        ->insertGetId([
            'user_id' => $request->user()->id,
            'product_type' => "FLIGHT",
            'reference_id' => $retreive->refId,
            'total_amount' => $retreive->payDet->fareDet->total->amount,
            'status' => $retreive->status,
            'created_at' => date('Y-m-d h:m:i'),
            'updated_at' => date('Y-m-d h:m:i')
        ]);

        DB::table('booking_flights')
        ->insert([
            'booking_id' => $booking_id,
            'booking_data' => json_encode($booking_data),
            'passenger_data' => json_encode($request->pd),
            'review_data' => json_encode($review),
            'retreive_data' => json_encode($retreive),
            'created_at' => date('Y-m-d h:m:i'),
            'updated_at' => date('Y-m-d h:m:i') 
        ]);

        return response()->json($retreive, 200);
        
    }

    public function _requestToVia($endpoint, $data, $type = "POST") {
        $endpoint = "http://testph.via.com/apiv2/" . $endpoint;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Via-Access-Token:c51b5435-cec6-4de2-b133-4t9e3s12ta02',
            'Content-Type:application/json'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        
        if($type == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        $content = curl_exec($ch);
        curl_close($ch);

        $res = json_decode($content);
        $res->webDta = [
            "dev" => "carlo.flores@complexus.net"
        ];

        return $res;
    }
}
