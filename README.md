
## About BTT Product API

Barkada Travel and Tours will be able to provide an API for their product in order to cater a Partner.

## Products

- Flights
- Hotels
- Tours


## Documentation
- **[Postman Documentation](https://documenter.getpostman.com/view/8469684/SWLe67Ru?version=latest)**
