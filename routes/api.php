<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::group(['middleware' => 'auth:api'], function() {
    Route::get('/flights/autofill', 'FlightsController@autofill'); #->middleware('scope:can-search-flights');
    Route::get('/flights/search', 'FlightsController@search'); #->middleware('scope:can-search-flights');
    Route::post('/flights/review', 'FlightsController@review'); #->middleware('scope:can-search-flights');
    Route::post('/flights/book', 'FlightsController@book'); #->middleware('scope:can-search-flights');

    Route::get('/countries', 'GeneralController@countries'); #->middleware('scope:can-search-flights');
// });